import React from 'react';
import moment from 'moment';
import './calendar.css';

const event = [
    {
        date: '18-03-2019',
        events: [
            {name: 'event name', body: 'event body', time: '11:00'},
            {name: 'event name2', body: 'event bod2', time: '12:00'}
        ]
    },
    {
        date: '23-02-2019',
        events: [
            {name: 'event 3', body: 'event 3', time: '12:00'},
            {name: 'event name2', body: 'event bod2', time: '12:00'}
        ]
    },
    {
        date: '22-02-2019',
        events: [
            {name: 'event 4', body: 'event 3', time: '12:00'}
        ]
    },
    {
        date: '20-03-2019',
        events: [
            {name: 'other month', body: 'other month', time: '00:00'}
        ]
    }
];

export default class Calendar extends React.Component {
    state = {
        formShow: false,
        showModeSelector: false,
        chevron: " fa-chevron-down",
        mode: true,

        startWeek: moment(this.selectedDay),
        selectedDay: moment(),
        dateContext: moment(),
        events: event,
        name: "",
        body: "",
        time: "",
    }

    constructor(props) {
        super(props);
        this.width = props.width || "350px";
        this.style = props.style || {};
        this.style.width = this.width;
        this.nameChange = this.nameChange.bind(this);
        this.textChange = this.textChange.bind(this);
        this.timeChange = this.timeChange.bind(this);
    }


    weekdaysShort = moment.weekdaysShort();

    year = () => {
        return this.state.dateContext.format("Y");
    }
    month = () => {
        return this.state.dateContext.format("MM");
    }

    daysInMonth = () => {
        return this.state.dateContext.daysInMonth();
    }

    currentDay = () => {
        return this.state.dateContext.format("D");
    }

    firstDayOfMonth = () => {
        let dateContext = this.state.dateContext;
        let firstDay = moment(dateContext).startOf('month').format('d');
        return firstDay;
    }


    nextMonth = () => {
        let dateContext = Object.assign({}, this.state.dateContext);
        dateContext = moment(dateContext).add(1, "month");
        this.setState({
            dateContext: dateContext
        });
        this.props.onNextMonth && this.props.onNextMonth();

    }


    prevMonth = () => {
        let dateContext = Object.assign({}, this.state.dateContext);
        dateContext = moment(dateContext).subtract(1, "month");
        this.setState({
            dateContext: dateContext
        });
        this.props.onPrevMonth && this.props.onPrevMonth();


    }
    nextWeek = () => {
        let dateContext = this.state.startWeek;
        dateContext = moment(dateContext).add(1, "w");
        this.setState({

            startWeek: moment(dateContext).startOf("week")
        });
        this.props.onNextWeek && this.props.onNextWeek();

    }


    prevWeek = () => {
        let dateContext = this.state.startWeek;
        dateContext = moment(dateContext).subtract(1, "w");
        this.setState({

            startWeek: moment(dateContext).startOf("week")
        });
        this.props.onNextWeek && this.props.onNextWeek();

    }

    getPrevMonth = () => {
        let dateContext = Object.assign({}, this.state.dateContext);
        dateContext = moment(dateContext).subtract(1, "month");
        return dateContext.format("MMM");

    }

    getNextMonth = () => {
        let dateContext = Object.assign({}, this.state.dateContext);
        dateContext = moment(dateContext).add(1, "month");
        return dateContext.format("MMM");

    }


    onDayClick = (e, day) => {

        this.setState({
            selectedDay: moment(day, "D"),
            startWeek: moment(day, "D")

        }, () => {
            console.log("SELECTED DAY: ", this.state.selectedDay);
        });
        this.selectedDay = day;
        this.props.onDayClick && this.props.onDayClick(e, day);
    }

    formShow() {


        this.setState({
            formShow: !this.state.formShow
        })

    }


    nameChange(event) {
        this.setState({name: event.target.value});
    }

    textChange(event) {
        this.setState({body: event.target.value});
    }

    timeChange(event) {
        this.setState({time: event.target.value});
    }

    Form = () => {
        return (

            <div className="addEvent">
                <div className="card-body">
                    <h6 className="addHeader"><i className="fas fa-times" onClick={(e) => this.formShow()}></i>Add new
                        event<i className="fas fa-check" onClick={() => +this.addEvent()}></i></h6>
                </div>
                <form>
                    <div className="form-group">
                        <label htmlFor="exampleInputEmail1">Name of event</label>
                        <input type="text" value={this.state.name} className="form-control"
                               placeholder="Event`s name" onChange={this.nameChange}/>
                    </div>
                    <div className="form-group">
                        <label>Event`s notes</label>
                        <textarea type="text" className="form-control"
                                  value={this.state.body} placeholder="Text" onChange={this.textChange}/>
                    </div>
                    <div className="form-group">
                        <label>Time</label>
                        <input type="time" className="form-control" value={this.state.time} onChange={this.timeChange}/>
                    </div>


                </form>
            </div>
        );

    }

    modeSelector = () => {
        return (
            this.state.showModeSelector ?
                <div className="card card-body pnlModeSelector">
                    <div className="btnModeSelector" onClick={() => this.selectModeMonth()}>This week</div>
                    <div className="btnModeSelector" onClick={() => this.selectModeMonth()}>This month</div>
                </div> : ""

        );

    }


    selectModeMonth = () => {
        this.setState({
            mode: !this.state.mode,

        });
        this.showModeSelector();

    }

    showModeSelector = () => {
        this.setState({
            showModeSelector: !this.state.showModeSelector
        });

        if (this.state.showModeSelector) {
            this.setState({
                chevron: "fa-chevron-down"
            });
        } else {
            this.setState({
                chevron: "fa-chevron-up"
            });
        }

    }

    addEvent = () => {

        const e = [...this.state.events];
        e.push({
            date: moment(this.state.selectedDay).format("DD-MM-YYYY"),
            events: [{name: this.state.name, body: this.state.body, time: this.state.time}]
        });
        this.setState({
            events: e,
        });
        event.push(e.pop());
        this.formShow();
        console.log(this.state.events);

    }


    Events = () => {
        let evens;
        if (this.state.mode === true) {
            if (this.month() == moment(this.state.dateContext).format("MM")) {
                evens = this.state.events.filter((d) => {
                    return moment(d.date, 'DD-MM-YYYY').isBetween(moment(this.state.dateContext).subtract(1, "d"), moment(this.state.dateContext).endOf('month'));
                });

            } else {
                evens = this.state.events.filter((d) => {
                    return moment(d.date, 'DD-MM-YYYY').isBetween(moment(this.state.selectedDay).startOf('month').subtract(1, 'day'), moment(this.state.selectedDay).endOf('month'));
                });
            }
        } else if (this.state.mode === false) {
            console.log("12222", this.state.selectedDay);
            if (moment(this.selectedDay).isSame(this.state.dateContext, 'week')) {
                evens = this.state.events.filter((d) => {
                    return moment(d.date, 'DD-MM-YYYY').isBetween(moment(this.state.dateContext).subtract(1, "d"), moment(this.state.dateContext).endOf('week'));
                });
            } else {
                evens = this.state.events.filter((d) => {
                    return moment(d.date, 'DD-MM-YYYY').isBetween(moment(this.state.selectedDay).startOf('week'), moment(this.state.selectedDay).endOf('week'));
                });
            }
        }
        return evens.map((d, index) => {
            return (
                <div className='event-container' key={index}>
                    <div className='date'>
                        <h2>{moment(d.date, 'DD-MM-YYYY').format("ddd, D MMM")}</h2>
                    </div>
                    <div className='evens'>
                        {d.events
                            .map((even, index) => {
                                return (
                                    <div
                                        key={index}>
                                        <div className="name-time">
                                            <h5>{even.name}</h5>
                                            <span>{even.time}</span>
                                        </div>
                                        <div className={"event-body"}>
                                            <p>{even.body}</p>

                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            )
        });


    }


    render() {


        let weekdays = this.weekdaysShort.map((day) => {
            let ClassName = ((day === "Sun") || (day === "Sat") ? "weekend" : "");
            return (<td key={day} className="week-day" className={ClassName}>{day}</td>)

        });
        //-------------render month----------------------------


        let blanks = [];
        for (let i = 0; i < this.firstDayOfMonth(); i++) {
            blanks.push(
                <td key={i * 80} className="emptySlot">{}</td>
            );
        }
        let daysInMonth = [];

        for (let d = 1; d <= this.daysInMonth(); d++) {


            let i = moment(this.state.dateContext).add(d - 4, "d").format("ddd");
            let className2 = (d == moment(this.state.selectedDay).format("DD") ? "selected-day " : "");
            let className = (d == this.currentDay() ? "day current-day" : "day");
            let className3 = ((i === "Sun" || i === "Sat") ? "weekend" : "");
            daysInMonth.push(
                <td key={d} className={className2 + className}>
                    <span onClick={(e) => {
                        this.onDayClick(e, d)
                    }} className={className3}>{d}</span>


                </td>
            );


        }

        var totalSlots = [...blanks, ...daysInMonth];
        let rows = [];
        let cells = [];

        totalSlots.forEach((row, i) => {

            if ((i % 7) !== 0) {

                cells.push(row)
            } else {
                let insertRow = cells.slice();
                rows.push(insertRow);
                cells = [];
                cells.push(row);
            }
            if (i === totalSlots.length - 1) {
                let insertRow = cells.slice();
                rows.push(insertRow);
            }
        });
        let Elems = rows.map((d, i) => {

            return (
                <tr key={i * 100}>{d}</tr>
            );
        });
        //-------------------------------end render month---------------------

        //----------------render week-----------------------------------
        let startOfWeek;
        startOfWeek = this.state.startWeek;
        console.log("ts", startOfWeek)
        const days = [];
        let d, day = startOfWeek;

        for (let i = 0; i < 7; i++) {

            d = moment(day).format("DD");
            let className2 = (d == moment(this.state.selectedDay).format("D") ? "selected-day " : "");
            let className = (d == this.currentDay() ? "day current-day" : "day");
            let className3 = ((i === 0 || i === 6) ? "weekend" : "");
            days.push(
                <td className={className2 + className} onClick={(e) => {
                    this.onDayClick(e, d)
                }}>
                    <span className={className3}>{d}</span>
                </td>
            );
            day = day.clone().add(1, 'days');
        }
        //----------------------end render week---------------------


        return (
            !this.state.formShow ?
                <div>

                    <div className="calendar-container">

                        <nav className="navbar navbar-expand-lg navbar-dark bg-dark  " style={{borderRadius: "3px"}}>
                            <a className="navbar-brand" href="#">Calendar</a>

                            <form className="form-inline my-2 my-lg-0"
                                  style={{borderRadius: "950%", paddingLeft: "75%"}}>
                                <button className="btn btn-outline-success my-2 my-sm-0" type="button"
                                        onClick={() => this.formShow()}
                                        style={{borderRadius: "950%"}}><strong>
                                    <i
                                        className="fas fa-plus"></i></strong></button>
                            </form>
                        </nav>
                        <table className="calendar">
                            <thead>
                            <tr className="calendar-header">
                                {this.state.mode ?
                                    <td className="nav-month">
                                        <i className=" prev fa fa-fw " onClick={(e) => {
                                            this.prevMonth()
                                        }}>{this.getPrevMonth()}</i>
                                        <i className="month-label " onClick={(e) => {
                                            this.showModeSelector()
                                        }}>{moment(this.state.dateContext).format("MMMM")}{" "}{this.year()}<span
                                            className={"fa fa-fw " + this.state.chevron}></span></i>
                                        <i className=" next fa fa-fw " onClick={(e) => {
                                            this.nextMonth()
                                        }}>{this.getNextMonth()}</i>
                                    </td>
                                    :
                                    <td className="nav-month">
                                        <i className=" prev fa fa-fw " onClick={(e) => {
                                            this.prevWeek()
                                        }}>Prev</i>
                                        <i className="month-label" onClick={(e) => {
                                            this.showModeSelector()
                                        }}>{this.year()}<span
                                            className={"fa fa-fw " + this.state.chevron}></span></i>
                                        <i className=" next fa fa-fw " onClick={(e) => {
                                            this.nextWeek()
                                        }}>Next</i>
                                    </td>
                                }
                            </tr>

                            </thead>
                        </table>
                        <this.modeSelector/>
                        <table className="calendar">
                            {
                                !this.state.showModeSelector ?
                                    <tr className="week">{weekdays}</tr> :
                                    ""
                            }
                            <tbody>
                            {this.state.mode ? Elems : days}

                            </tbody>
                        </table>
                    </div>
                    <div className={"events"}>
                        <this.Events/>
                    </div>
                </div> : <this.Form/>
        );
    }
}
