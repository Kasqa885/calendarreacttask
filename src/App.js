import React, {Component} from 'react';
import './App.css';
import moment from 'moment';
import Calendar from './Components/Calendar/';

const style ={

    pasition:"relative",
    margin:"50px auto",
}

class App extends Component {

    constructor() {
        super();

        this.state = {
            name: ""
        }
    }

    onDayClick=(e,day)=>{
        //alert(day);
    }
    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-3"></div>
                    <div className="col-6">


                        <Calendar style={style} width="302px" onDayClick={(e,day)=>this.onDayClick(e,day)}/>
                    </div>
                </div>
            </div>

        );
    }
}

export default App;
